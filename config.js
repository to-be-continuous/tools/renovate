module.exports = {
  "platform": "gitlab",
  "dependencyDashboardAutoclose": true,
  "repositoryCache": "enabled",
  "autodiscover": true,
  "extends": ["mergeConfidence:all-badges"],
  "ignorePrAuthor": true,
  "onboardingConfig": {
    "extends": ["config:recommended", ":disableDependencyDashboard"]
  }
};
